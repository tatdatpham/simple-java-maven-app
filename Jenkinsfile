pipeline {
    agent { label 'maven' }
    options {
        skipStagesAfterUnstable()
    }

    environment {
        NVD_API_KEY = credentials('NVD_API_KEY') // Replace with the ID of the Jenkins credential
    }
    

    stages {
        stage('Build') {
            steps {
                sh 'mvn -B -DskipTests clean package'
            }
            post {
                success {
                    script {
                        env.BUILD_SUCCESS = true
                    }
                }
            }
        }
        stage('Test') {
            steps {
                sh 'mvn test'
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }

        stage('SCA') {
            when {
                expression {
                    return env.BUILD_SUCCESS == 'true'
                }
            }
            steps {
                script {
                    dependencyCheck additionalArguments: '--scan src --format XML --out target --nvdApiKey ${env.NVD_API_KEY} --data /usr/share/dependency-check/data', odcInstallation: 'OWASP-Dependency-Check'
                }
            }
            post {
                always {
                    dependencyCheckPublisher pattern: 'target/dependency-check-report.xml'
                }
            }
        }

        stage('SAST') {
            steps {
                script {
                    def scannerHome = tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
                    withSonarQubeEnv() {
                        sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=tatdatpham_simple-java-maven-app_AY-b0cN30Ha0ksPS5rmH -Dsonar.sources=src -Dsonar.java.binaries=target/classes"
                    }
                }
            } 
        }


        stage('Coverage') {
            steps {
                sh 'mvn jacoco:prepare-agent test jacoco:report' // generate JaCoCo coverage report
                jacoco(execPattern: '**/target/jacoco.exec', classPattern: '**/target/classes', sourcePattern: '**/src/main/java')
            }
        }

        stage("DAST") {
            steps {
                script {
                    // Generate the report file name with the node name prefix
                    def reportFileName = "${env.NODE_NAME}-test-report.html"
                    sh """
                        docker run -u root -v /opt/devsecops-docker/owasp-zap-reports:/zap/wrk:rw --rm -t ghcr.io/zaproxy/zaproxy:stable zap-baseline.py -t https://tatdatpham.me -r ${reportFileName}
                    """
                }
            }
            post {
                always {
                    script {
                        // Copy the report file to the current working directory
                        sh "cp /tmp/${env.NODE_NAME}-test-report.html \$(pwd)/${env.NODE_NAME}-test-report.html"
                        // List files to see if the report is there
                        sh 'ls -al \$(pwd)' 
                    }
                    // Archive the artifact
                    archiveArtifacts artifacts: "${env.NODE_NAME}-test-report.html", fingerprint: true
                    // Publish the HTML report
                    publishHTML([
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: "${env.WORKSPACE}",
                        reportFiles: "${env.NODE_NAME}-test-report.html",
                        reportName: 'OWASP ZAP Report'
                    ])
                    script {
                        // Delete the report file and zap.xml and report in host folder: /opt/devsecops-docker/owasp-zap-reports
                        sh "rm -f /tmp/${env.NODE_NAME}-test-report.html && rm -f /tmp/zap.yaml"
                    }
                }
            }
        }
  

        
        stage('Publish') {
            steps {
                publishHTML(target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'target/site/jacoco',
                    reportFiles: 'index.html',
                    reportName: 'JaCoCo Code Coverage'
                ])
            }
        }


        // stage('Deliver') { 
        //     steps {
        //         sh 'echo "Deliver"' 
        //     }
        // }
    }
    post {
        failure {
            updateGitlabCommitStatus(name: "\${env.STAGE_NAME}", state: 'failed')
        }
        unstable {
            updateGitlabCommitStatus(name: "\${env.STAGE_NAME}", state: 'failed')
        }
        success {
            updateGitlabCommitStatus(name: "\${env.STAGE_NAME}", state: 'success')
        }
        aborted {
            updateGitlabCommitStatus(name: "\${env.STAGE_NAME}", state: 'canceled')
        }
        always { 
            deleteDir()                     // clean up workspace
            dir("${WORKSPACE}@tmp") {       // clean up tmp directory
                deleteDir()
            }
            dir("${WORKSPACE}@script") {    // clean up script directory
                deleteDir()
            }
        }
    }
}